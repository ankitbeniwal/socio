@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
					
					<a href="{{ route('all_posts') }}" class="btn btn-success float-right ml-5">Show all Posts</a>
					<a href="{{ route('create_post') }}" class="btn btn-primary float-right">Add Post</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

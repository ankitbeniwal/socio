<?php

namespace App\Http\Controllers;

use App\Blog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $number_of_posts = Blog::count();
        $new_posts = Blog::orderBy('id', 'DESC')->take(5)->get();

        $data = [
            'post_count' => $number_of_posts,
            'new_posts' => $new_posts,
        ];

        return view('home', $data);
    }
	
	public function PostList()
    {
        $number_of_posts = Blog::count();
        $new_posts = Blog::get();

        $data = [
            'post_count' => $number_of_posts,
            'posts' => $new_posts,
        ];

        return view('post_list', $data);
    }
	
	public function createPost()
    {
        return view('post_create');
    }
	
	public function storePost(Request $request)
    {
		$article = new Blog();
        $article->body = $request->get('body');
        $article->author = Auth::id();
        $image = $request->file('image');
		if($image){
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			$article->image = $input['imagename'];
		}
        $article->save();
		if($image){
			$destinationPath = public_path('/images');
			$image->move($destinationPath, $input['imagename']);
		}
        return redirect()->route('all_posts')->with('status', 'New article has been successfully created!');
    }
}
